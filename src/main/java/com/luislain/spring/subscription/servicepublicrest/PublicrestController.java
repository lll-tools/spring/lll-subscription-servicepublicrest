package com.luislain.spring.subscription.servicepublicrest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
// import org.springframework.boot.web.client.RestTemplateBuilder;
// import org.springframework.context.annotation.Bean;

@RestController
@RequestMapping(path = "/api")
public class PublicrestController {

    private String subscriptionServiceURL = "http://serviceproxy:8080/";
    //private String subscriptionServiceURL = "http://localhost:8181/";
    // private String subscriptionServiceURL2 = "http://localhost:8282/";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/add")
    public Subscription addSubscription(@PathVariable Long id) {
        Subscription subscription = restTemplate.getForObject(
            subscriptionServiceURL + "api/add?id=" + id, Subscription.class);
        return subscription;
    }

    @GetMapping("/cancel/{id}")
    public Subscription cancelSubscription(@PathVariable Long id) {
        Subscription subscription = restTemplate.getForObject(
            subscriptionServiceURL + "api/cancel?id=" + id, Subscription.class);
        return subscription;
    }

    //, produces = "application/json"
    @GetMapping("/details/{id}")
    public Subscription getSubscription(@PathVariable Long id) {
        Subscription subscription = restTemplate.getForObject(
            subscriptionServiceURL + "api/details?id=" + id, Subscription.class);
        return subscription;
    }

    @GetMapping("/subscriptions")
    public Subscription[] getAllSubscriptions() {
        Subscription[] subscriptions = restTemplate.getForObject(
            subscriptionServiceURL + "api/all", Subscription[].class);
        return subscriptions;
    }

}
