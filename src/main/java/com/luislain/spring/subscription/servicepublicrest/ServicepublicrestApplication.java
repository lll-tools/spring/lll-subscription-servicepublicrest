package com.luislain.spring.subscription.servicepublicrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
// import org.slf4j.Logger;
// import org.slf4j.LoggerFactory;
// import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
public class ServicepublicrestApplication {

	//private static final Logger log = LoggerFactory.getLogger(ServicepublicrestApplication.class);

	public static void main(String[] args) {

		SpringApplication.run(ServicepublicrestApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}

	// @Bean
	// public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
	// 	return args -> {
	// 		Subscription[] subscriptions = restTemplate.getForObject(
	// 				"http://localhost:8181/api/all", Subscription[].class);
			
	// 		for (Subscription subscription : subscriptions) {
	// 			log.info(subscription.toString());
	// 		}
	// 	};
	// }
}
